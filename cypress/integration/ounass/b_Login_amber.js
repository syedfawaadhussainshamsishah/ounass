describe('To check the Login/ Sign Up Functionality for English language', function () {

    beforeEach(() => {
        // run these tests as if in a desktop
        // browser with a 720p monitor
        cy.viewport(1280, 720)
    })

    it('To check the Login Functionality',
        {
            retries: {
                runMode: 4,
                openMode: 4
            }
        }, function () {
            // Cypress.config('pageLoadTimeout', 100000)
            Cypress.on('uncaught:exception', (err, runnable) => {
                return false
            })
            cy.visit('https://www.ounass.ae/', { failOnStatusCode: false });
            cy.wait(1000);
        })



    it('Click on to User icon in top left', function () {
        cy.wait(4000);
        cy.get('#root > div > div.Base-content > div.ProductNavigation > div.Popup.CustomerPopup > button > div').should('be.visible').click();
    })

    it('Click Amber button', function () {
        cy.wait(4000);
        cy.get('body > div:nth-child(33) > div > div > div.SignInThirdpartyButtons > button.SignInThirdpartyButtons-amberButton').should('be.visible').click();
    })

    it('Click Facebook connect', function () {
        cy.visit('https://www.ounass.ae')
        cy.wait(4000);
        cy.get('#root > div > div.Base-content > div.ProductNavigation > div.Popup.CustomerPopup > button > div').should('be.visible').click();
        cy.contains('CONTINUE WITH FACEBOOK').should('be.visible').click() // Assert that el is visible
        cy.wait(4000);
    })

    it('Login with Facebook account', function () {
        // cy.visit('https://www.ounass.ae')
        cy.wait(4000);
        cy.get('#email').should('be.visible').type('syed.fawaad@arkhitech.com');
        cy.get('#pass').should('be.visible').type('fawaad123');
        cy.get('#loginbutton').should('be.visible').click();
        cy.wait(4000);
    })


    it('verify user is successfully logged in', function () {
        cy.wait(4000);
        cy.get('#root > div > div.Base-content > div.ProductNavigation > div.Popup.CustomerPopup > button > div').should('be.visible').click();
        cy.get('#root > div > div.Base-content > div.Navigation-content > div.Account > div.Account-content.MyAccountPage > div:nth-child(4) > a').click();
        cy.wait(2000);
        cy.get('input').should('be.disabled', 'syed.fawaad@arkhitech.com')
    });


    it('After 72 items, “View More”', function () {

        function loadmore(){
            var numitems = 1;
            var _url = 'https://www.ounass.ae/clothing/'+numitems+'&include_rts=1';
            $.getJSON(_url,function(data){
            for(var i = 0; i<=72 & numitmes<=12; i++){
                    var loadmorebutton = data[i].created_at;
                    loadmorebutton.click();
                }
            });
        }
        loadmore();
    });

    it('After selecting “Christian Siriano” from filters only 2 results are found', function () {
        cy.visit('https://www.ounass.ae/women')
        cy.wait(4000);
        cy.get('#root > div > div.Base-content > div.ProductNavigation > div:nth-child(2) > div > div.QuickSearch-v2-inputContainer.has-border > form > input').should('be.visible').type('Christian Siriano{enter}');
    });

    it('Verify products only with title “Christian Sirano” as a brand is displayed', function () {
        cy.visit('https://www.ounass.ae/women')
        cy.wait(4000);
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.PLP-facetsAndResults > div.PLP-results > div.PLP-productList.PLP-productList--3 > div:nth-child(1) > div > a > img.Product-hoverImage').should('have','Christian Siriano');

        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.PLP-facetsAndResults > div.PLP-results > div.PLP-productList.PLP-productList--3 > div:nth-child(2) > div > a > img.Product-hoverImage').should('have','Christian Siriano');

    });


});