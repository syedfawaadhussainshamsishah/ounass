describe('To check the Login/ Sign Up Functionality for English language', function () {

    it('To check the Login Functionality',
        {
            retries: {
                runMode: 4,
                openMode: 4
            }
        }, function () {
            // Cypress.config('pageLoadTimeout', 100000)
            Cypress.on('uncaught:exception', (err, runnable) => {
                return false
            })
            cy.visit('https://www.ounass.ae/', { failOnStatusCode: false });
            cy.wait(1000);
            // cy.url().should('eq', 'https://www.ounass.ae/');
        })

    it('Show Sign Up in Main Page', function () {
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })
        cy.get('#root > div > div.Base-content > div.ProductNavigation > div.Popup.CustomerPopup > button > div').should('be.visible').click();
        cy.wait(2000);
    });


    it('Move to Sign Up from Main Page', function () {
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })
        // Using cy.visit because there is an error register button is disabled not working 
        cy.visit('https://www.ounass.ae/customer/register')
        cy.wait(2000);
    });


    it('To check the availability of Email, Password text-boxes and Submit button ', function () {
        cy.visit('https://www.ounass.ae/customer/register')
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.Register-content > div.Register-leftHandSide > form > div.Profile-row.first > div.Profile-firstNameWrapper > input').should('be.visible');
        cy.wait(2000);
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.Register-content > div.Register-leftHandSide > form > div.Profile-row.first > div.Profile-lastNameWrapper > input').should('be.visible');
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.Register-content > div.Register-leftHandSide > form > div.Profile-emailWrapper > input').should('be.visible');
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.Register-content > div.Register-leftHandSide > form > div.PasswordField.PasswordField__profileForm > input').should('be.visible');
        cy.wait(2000);
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.Register-content > div.Register-leftHandSide > form > button').should('be.visible');
    });

    it('Click on Submit button without Email and Password', function () {
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.Register-content > div.Register-leftHandSide > form > button').click().should('be.visible');
        cy.wait(2000);
    })



    it('Sign Up with correct fields', function () {
        cy.visit('https://www.ounass.ae/customer/register')
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.Register-content > div.Register-leftHandSide > form > div.Profile-row.first > div.Profile-firstNameWrapper > input').should('be.visible').type('Syed Fawaad');
        cy.wait(2000);
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.Register-content > div.Register-leftHandSide > form > div.Profile-row.first > div.Profile-lastNameWrapper > input').should('be.visible').type('Hussain');
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.Register-content > div.Register-leftHandSide > form > div.Profile-emailWrapper > input').should('be.visible').type('syedfawaadhussainshamsishah@yahoo.com');
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.Register-content > div.Register-leftHandSide > form > div.PasswordField.PasswordField__profileForm > input').type('Fawaad123');
        cy.wait(2000);
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.Register-content > div.Register-leftHandSide > form > button').should('be.visible').click();
    });



    //    Login 

    it('Show Login In in Main Page', function () {
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })
        cy.get('#root > div > div.Base-content > div.ProductNavigation > div.Popup.CustomerPopup > button > div').should('be.visible').click();
        cy.wait(2000);
    });



    it('Login with correct fields', function () {
        cy.visit('https://www.ounass.ae/');
        cy.wait(4000);
        cy.get('#root > div > div.Base-content > div.ProductNavigation > div.Popup.CustomerPopup > button > div').should('be.visible').click();

        cy.xpath('/html/body/div[3]/div/div/form/div[1]/input').click().type('syedfawaadhussainshamsishah@yahoo.com');
        cy.xpath('/html/body/div[3]/div/div/form/div[2]/input').type('Fawaad123');
        cy.wait(2000);
        cy.xpath('/html/body/div[3]/div/div/form/button').should('be.visible').click();
    });


    it('Verify that after logging into the account the “Email-Address” field is un-editable', function () {
        cy.wait(4000);
        cy.get('#root > div > div.Base-content > div.ProductNavigation > div.Popup.CustomerPopup > a > div').should('be.visible').click();
        cy.get('#root > div > div.Base-content > div.Navigation-content > div.Account > div.Account-content.MyAccountPage > div:nth-child(4) > a').click();
        cy.wait(2000);
        cy.get('#root > div > div.Base-content > div.Navigation-content > div.Account > div.Account-content.MyProfile > form.MyProfile-profileForm > div.Profile-emailWrapper > input').should('be.disabled')
    });



    it('Verify it is the same email with which you registered the account', function () {
        cy.wait(4000);
        // cy.visit('https://www.ounass.ae/');
        cy.get('input').should('be.disabled', 'syedfawaadhussainshamsishah@yahoo.com')
    });


    it('Update the phone number to “+97167324238” and verify it is updated', function () {
       //There is no option to update phone number in customer profile
    });


    it('Add to Bag', function () {
        cy.wait(4000);
        cy.visit('https://www.ounass.ae/');
        cy.get('#root > div > div.Base-content > header > div.SiteNavigation-l1Categories > a:nth-child(2)').should('be.visible').click();
        cy.wait(2000);

        // add clothes 
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.BoxSlider.BoxSlider--150x120 > div > div > div.swiper-wrapper > a.Banner.swiper-slide.swiper-slide-active > div > img').click();
        cy.wait(2000);
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.PLP-facetsAndResults > div.PLP-results > div.PLP-productList.PLP-productList--3 > div:nth-child(1) > div > a > img.Product-hoverImage').click();
        // size
        cy.get('#react-select-2--value').click();

        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.PDP-imagesAndBrief > div.Brief > div.Brief-interactions > div.Brief-actions > button.AddToBag').click();


        // add shoes
        cy.get('#root > div > div.Base-content > div.CategoryNavigation-wrapper > nav > a:nth-child(5) > span').click();
        cy.wait(2000);
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.PLP-facetsAndResults > div.PLP-results > div.PLP-productList.PLP-productList--3 > div:nth-child(1) > div > a > img.Product-hoverImage').click();

        // colour
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.PDP-imagesAndBrief > div.Brief > div.Brief-interactions > div.SelectConfigurables > div.ColorSelection > div.ColorSelection-options.showLess > button:nth-child(1)').click();

        // size
        cy.get('#react-select-2--value').click();

        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.PDP-imagesAndBrief > div.Brief > div.Brief-interactions > div.Brief-actions > button.AddToBag').click({force: true});


        // add watches
        cy.get('#root > div > div.Base-content > div.CategoryNavigation-wrapper > nav > a:nth-child(7) > span').click();
        cy.wait(2000);
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.PLP-facetsAndResults > div.PLP-results > div.PLP-productList.PLP-productList--3 > div:nth-child(2) > div > a > img.Product-hoverImage').click();

        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.PDP-imagesAndBrief > div.Brief > div.Brief-interactions > div.Brief-actions > button.AddToBag').click({force: true});


        // add bags
        cy.get('#root > div > div.Base-content > div.CategoryNavigation-wrapper > nav > a:nth-child(10) > span').click();
        cy.wait(2000);
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.PLP-facetsAndResults > div.PLP-results > div.PLP-productList.PLP-productList--3 > div:nth-child(1) > div > a > img.Product-hoverImage').click();

        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div > div.PDP-imagesAndBrief > div.Brief > div.Brief-interactions > div.Brief-actions > button.AddToBag').click({force: true});


        // move to bags 
        cy.get('#root > div > div.Base-content > div.ProductNavigation > div.MiniCartPopup-Outside > div > a').click();

        // secure checkout
        cy.get('#root > div > div.Base-content > div.Navigation-content.has-margin > div.Cart > div > div.Cart-rightSide > div.OrderSummary.Cart > div > div.CartTotal.Cart > div.CartTotal-actions > button').click();

        // delivery information
        cy.get('#react-select-2--value > div.Select-value').click()
               .should('be.visible').type('+92{enter}');

        cy.get('#mobileNumber').should('be.visible').type('567324238');
        // cy.get('#react-select-3--value').select('Abu Dhabi');

        // cy.xpath('//*[@id="root"]/div/div[1]/div/div[3]/div[1]/form/section[1]/label[6]/input').click().should('be.visible').type('test');
        // cy.get('#root > div > div.Base-content > div > div.Checkout-content > div.Delivery-signInAndShippingAddress > form > section.NewAddress > label:nth-child(6)').then(($iframe)=>{
        //     const $input = $iframe.contents().find('body').find('div').find('input');
        //     let data = cy.wrap($input)
        //     data.type('JavaS');
        //     data.type('{downarrow}');
        //     data.type('{downarrow}').click();
        //   })
        cy.get('#street').should('be.visible').type('test');
        cy.get('#additionalInformation').should('be.visible').type('test');

        // continue
        cy.get('#root > div > div.Base-content > div > div.Checkout-content > div.Delivery-signInAndShippingAddress > form > button').should('be.visible').click();

            
    });

})