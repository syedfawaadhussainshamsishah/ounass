// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************
require('cypress-xpath')
// Import commands.js using ES2015 syntax:
import './commands'

module.exports = (on, config) => {
    on('before:browser:launch', (browser = {}, launchOptions) => {
      // `args` is an array of all the arguments that will
      // be passed to browsers when it launches
      console.log(launchOptions.args) // print all current args
  
      if (browser.family === 'chromium' && browser.name !== 'electron') {
        // auto open devtools
        launchOptions.args.push('--auto-open-devtools-for-tabs')
      }
  
      if (browser.family === 'firefox') {
        // auto open devtools
        launchOptions.args.push('-devtools')
      }
  
      if (browser.name === 'electron') {
        // auto open devtools
        launchOptions.preferences.devTools = true
      }
  
      // whatever you return here becomes the launchOptions
      return launchOptions
    })
  }
// require('cypress-xpath')

// Alternatively you can use CommonJS syntax:
// require('./commands')
